<?php

namespace App\Repositories;

use App\Models\Orderdetail;
use App\Repositories\BaseRepository;

/**
 * Class OrderdetailRepository
 * @package App\Repositories
 * @version July 15, 2019, 2:51 pm UTC
*/

class OrderdetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Orderdetail::class;
    }
}
