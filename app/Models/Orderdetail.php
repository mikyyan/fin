<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Orderdetail
 * @package App\Models
 * @version July 15, 2019, 2:51 pm UTC
 *
 * @property string placeofrent
 * @property string leaseddays
 * @property string numberofcar
 * @property string description
 */
class Orderdetail extends Model
{
    use SoftDeletes;

    public $table = 'orderdetails';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'placeofrent',
        'leaseddays',
        'numberofcar',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'placeofrent' => 'string',
        'leaseddays' => 'string',
        'numberofcar' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'placeofrent' => 'required',
        'leaseddays' => 'required',
        'numberofcar' => 'required',
        'description' => 'required'
    ];

    
}
