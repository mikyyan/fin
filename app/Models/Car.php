<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Car
 * @package App\Models
 * @version July 15, 2019, 1:07 pm UTC
 *
 * @property string model
 * @property string type
 * @property string brand
 * @property string colour
 * @property string engine
 */
class Car extends Model
{
    use SoftDeletes;

    public $table = 'cars';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'model',
        'type',
        'brand',
        'colour',
        'engine'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'model' => 'string',
        'type' => 'string',
        'brand' => 'string',
        'colour' => 'string',
        'engine' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'model' => 'required',
        'type' => 'required',
        'brand' => 'required',
        'colour' => 'required',
        'engine' => 'required'
    ];

    
}
