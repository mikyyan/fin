<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Order
 * @package App\Models
 * @version July 15, 2019, 2:45 pm UTC
 *
 * @property string deliver
 * @property string return
 * @property string cost
 */
class Order extends Model
{
    use SoftDeletes;

    public $table = 'orders';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'deliver',
        'return',
        'cost'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'deliver' => 'string',
        'return' => 'string',
        'cost' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'deliver' => 'required',
        'return' => 'required',
        'cost' => 'required'
    ];

    
}
