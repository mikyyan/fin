<li class="{{ Request::is('cars*') ? 'active' : '' }}">
    <a href="{!! route('cars.index') !!}"><i class="fa fa-edit"></i><span>Cars</span></a>
</li>

<li class="{{ Request::is('orders*') ? 'active' : '' }}">
    <a href="{!! route('orders.index') !!}"><i class="fa fa-edit"></i><span>Orders</span></a>
</li>

<li class="{{ Request::is('orderdetails*') ? 'active' : '' }}">
    <a href="{!! route('orderdetails.index') !!}"><i class="fa fa-edit"></i><span>Orderdetails</span></a>
</li>

