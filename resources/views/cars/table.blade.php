<div class="table-responsive">
    <table class="table" id="cars-table">
        <thead>
            <tr>
                <th>Model</th>
        <th>Type</th>
        <th>Brand</th>
        <th>Colour</th>
        <th>Engine</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($cars as $car)
            <tr>
                <td>{!! $car->model !!}</td>
            <td>{!! $car->type !!}</td>
            <td>{!! $car->brand !!}</td>
            <td>{!! $car->colour !!}</td>
            <td>{!! $car->engine !!}</td>
                <td>
                    {!! Form::open(['route' => ['cars.destroy', $car->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('cars.show', [$car->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('cars.edit', [$car->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
