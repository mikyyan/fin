<!-- Model Field -->
<div class="form-group col-sm-6">
    {!! Form::label('model', 'Model:') !!}
    {!! Form::text('model', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Brand Field -->
<div class="form-group col-sm-6">
    {!! Form::label('brand', 'Brand:') !!}
    {!! Form::text('brand', null, ['class' => 'form-control']) !!}
</div>

<!-- Colour Field -->
<div class="form-group col-sm-6">
    {!! Form::label('colour', 'Colour:') !!}
    {!! Form::text('colour', null, ['class' => 'form-control']) !!}
</div>

<!-- Engine Field -->
<div class="form-group col-sm-6">
    {!! Form::label('engine', 'Engine:') !!}
    {!! Form::text('engine', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cars.index') !!}" class="btn btn-default">Cancel</a>
</div>
