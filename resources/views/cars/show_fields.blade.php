<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $car->id !!}</p>
</div>

<!-- Model Field -->
<div class="form-group">
    {!! Form::label('model', 'Model:') !!}
    <p>{!! $car->model !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $car->type !!}</p>
</div>

<!-- Brand Field -->
<div class="form-group">
    {!! Form::label('brand', 'Brand:') !!}
    <p>{!! $car->brand !!}</p>
</div>

<!-- Colour Field -->
<div class="form-group">
    {!! Form::label('colour', 'Colour:') !!}
    <p>{!! $car->colour !!}</p>
</div>

<!-- Engine Field -->
<div class="form-group">
    {!! Form::label('engine', 'Engine:') !!}
    <p>{!! $car->photo !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $car->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $car->updated_at !!}</p>
</div>

