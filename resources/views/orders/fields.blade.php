<!-- Deliver Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deliver', 'Deliver:') !!}
    {!! Form::text('deliver', null, ['class' => 'form-control']) !!}
</div>

<!-- Return Field -->
<div class="form-group col-sm-6">
    {!! Form::label('return', 'Return:') !!}
    {!! Form::text('return', null, ['class' => 'form-control']) !!}
</div>

<!-- Cost Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost', 'Cost:') !!}
    {!! Form::text('cost', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('orders.index') !!}" class="btn btn-default">Cancel</a>
</div>
