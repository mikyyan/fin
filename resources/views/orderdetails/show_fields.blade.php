<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $orderdetail->id !!}</p>
</div>

<!-- Place of rent Field -->
<div class="form-group">
    {!! Form::label('placeofrent', 'Place of rent:') !!}
    <p>{!! $orderdetail->placeofrent !!}</p>
</div>

<!-- Leased days Field -->
<div class="form-group">
    {!! Form::label('leaseddays', 'Leased days:') !!}
    <p>{!! $orderdetail->leaseddays !!}</p>
</div>

<!-- Number of car Field -->
<div class="form-group">
    {!! Form::label('numberofcar', 'Number of car:') !!}
    <p>{!! $orderdetail->numberofcar !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $orderdetail->description !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $orderdetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $orderdetail->updated_at !!}</p>
</div>

